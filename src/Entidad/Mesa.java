package Entidad;

import ufps.util.colecciones_seed.Cola;

public class Mesa {

    private int id_mesa;
    private Cola<Persona> sufragantes=new Cola();
    private Persona[] jurados=new Persona[3];

    public Mesa() {
    }

    public Mesa(int id_mesa) {
        this.id_mesa = id_mesa;
    }

    public int getId_mesa() {
        return id_mesa;
    }

    public void setId_mesa(int id_mesa) {
        this.id_mesa = id_mesa;
    }

    public Cola<Persona> getSufragantes() {
        return sufragantes;
    }

    public void setSufragantes(Cola<Persona> sufragantes) {
        this.sufragantes = sufragantes;
    }

    public Persona[] getJurados() {
        return jurados;
    }

    public void setJurados(Persona[] jurados) {
        this.jurados = jurados;
    }

    @Override
    public String toString() {
        return "\n Mesa{" + "id_mesa=" + id_mesa + '}';
    }
    
    
    public boolean asignarJurado(Persona p)
    {
    if(p.esTerceraEdad())
        return false;
    int i=buscarPosicionJurado();
    if(i!=-1)
        {
            this.jurados[i]=p;
            p.setEsJurado(true);
            return true;
        }
    return false;
    }
    
    private int buscarPosicionJurado()
    {
    for(int i=0;i<this.jurados.length;i++)
        {
            if(this.jurados[i]==null)
                return i;
        }
    return -1;
    }
    
    
}
